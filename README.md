<div align="center">
  <img src="https://gitlab.com/uploads/-/system/project/avatar/21393065/iconfinder_streetview_api_86432.png?width=64" alt="Dashboard Covid">
  <h3 align="center">Frontend Dashboard COVID</h3>
  <p align="center">
    Dashboard COVID API
  </p>
</div>

# DEMO

El resultado final del proyecto es el siguiente:

<div align="center">
  <img src="/img/dashboard.png" alt="Dashboard Covid">
</div>

## Prototipo

Para la elaboración del dashboard se conceptualizo con un prototipo realizado en Tableau con los datos provenientes de Serendipia, los cuales eran proporcionados por la API de Federico Garza

https://github.com/FedericoGarza/covidmx

<div align="center">
  <img src="/img/prototype.png" alt="Dashboard Covid">
</div>

Para su ejecución en conjunto con el backend ir al servicio Proxy y seguir las indicaciones

https://gitlab.com/dashboard-covid/proxy-server

Este servicio se puede ejecutar de forma independiente y levantar el microservicio con los siguientes comandos: 
Verifique que el compose.yml este correcto para su despliegue

# Herramientas

Se utilizaron los siguientes recursos para su elaboración

https://github.com/strotgen/mexico-leaflet
https://react-leaflet.js.org
https://react-google-charts.com

# Ramas

Basado en GitFlow

* Staging para despliegues en ambiente de pruebas
* Master para enviar a producción
* Ramas creadas por issues con merge request, para añadir caracteristicas, mejoras y corregir errores

# Estructura del proyecto

* **img** (Imagenes sobre la documentación)
* **enviroments** (Soporte para multiples ambientes en CI/CD)
* **nginx** (Configuración de producción para la aplicación react)
* **public** (Archivos publicos del proyecto)
* **src** (Directorio principal del proyecto, carpetas descriptivas manteniendo código limpio)

# Ambientes

La aplicación está preparada para ejecutarse en ambiente de desarrollo, producción y para pruebas

# Ejecución con Docker

## Aplicación de desarrollo 
```sh
docker-compose up
```

## Aplicación para producción
```sh
docker-compose -f docker-compose.prod.yml up
```

## Contribución

* Revisar el código de conducta

# Licencia

BSD 3-Clause "New" or "Revised" License
View in https://github.com/ActivandoIdeas/Cookiecutter-Django-AppEngine-GitLab/blob/master/LICENSE