const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = {
    entry: {
        app: path.resolve(__dirname, "src/index.js"),
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name].js",
        publicPath: "http://" + process.env.HOST + ":" + process.env.PORT + "/",
        chunkFilename: "js/[id].[chunkhash].js",
    },
    devServer: {
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        proxy: {
            '/api': {
                target: process.env.REACT_APP_API,
                pathRewrite: {
                    '^/api': ''
                },
            }
        },
        contentBase: path.resolve(__dirname, "dist"),
        host: '0.0.0.0',
        port: 9000,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        },
        disableHostCheck: true,
        open: false,
        overlay: true,
        compress: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm|webp$/,
                use: {
                    loader: "file-loader",
                    options: {
                        outputPath: "assets/"
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "public/index.html"),
            favicon: path.resolve(__dirname, "public/favicon.png"),
        }),
        new webpack.EnvironmentPlugin(['API'])
    ]
};
