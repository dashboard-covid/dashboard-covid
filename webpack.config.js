const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const TersetJSPlugin = require("terser-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const WorkboxWebpackPlugin = require("workbox-webpack-plugin");
const PreloadWebpackPlugin = require("preload-webpack-plugin");
const SitemapWebpackPlugin = require("sitemap-webpack-plugin").default;
const RobotstxtPlugin = require("robotstxt-webpack-plugin");
const {EnvironmentPlugin} = require("webpack");

const paths = [
    {
        path: "/",
        priority: "0.9"
    }
];

module.exports = {
    entry: {
        app: path.resolve(__dirname, "src/index.js")
    },
    devtool: false,
    performance: {
        maxAssetSize: 100000,
        maxEntrypointSize: 100000,
        hints: "warning"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name].[hash].js",
        publicPath: "/",
        chunkFilename: "js/[id].[chunkhash].js"
    },
    optimization: {
        minimizer: [new TersetJSPlugin(), new OptimizeCSSAssetsPlugin()]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm|webp$/,
                use: {
                    loader: "url-loader",
                    options: {
                        limit: 1000,
                        name: "[hash].[ext]",
                        outputPath: "assets"
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.API': JSON.stringify(process.env.API)
        }),
        new webpack.DllReferencePlugin({
            manifest: require("./modules-manifest.json")
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: ["**/app.*", "/assets/*"]
        }),
        new MiniCssExtractPlugin({
            filename: "css/[name].[hash].css",
            chunkFilename: "css/[id].[hash].css"
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "public/index.html"),
            favicon: path.resolve(__dirname, "public/favicon.png")
        }),
        new AddAssetHtmlPlugin({
            filepath: path.resolve(__dirname, "dist/js/*.dll.js"),
            outputPath: "js",
            publicPath: "/js"
        }),
        new WorkboxWebpackPlugin.GenerateSW({
            swDest: "sw.js",
            clientsClaim: true,
            skipWaiting: true,
            navigationPreload: true,
            runtimeCaching: [
                {
                    urlPattern: /\.*/,
                    handler: "NetworkFirst"
                }
            ]
        }),
        new PreloadWebpackPlugin({
            rel: "preload",
            as(entry) {
                if (/\.css$/.test(entry)) return "style";
                if (/\.woff$/.test(entry)) return "font";
                if (/\.woff2$/.test(entry)) return "font";
                if (/\.png$/.test(entry)) return "image";
                if (/\.webp$/.test(entry)) return "image";
                if (/\.svg$/.test(entry)) return "image";
                if (/\.jpg$/.test(entry)) return "image";
                return "script";
            }
        }),
        new SitemapWebpackPlugin("http://localhost", paths, {
            fileName: "sitemap.xml",
            skipGzip: true,
            lastMod: true,
            changeFreq: "monthly",
            priority: "0.4"
        }),
        new RobotstxtPlugin({
            policy: [
                {
                    userAgent: "Googlebot",
                    allow: "/",
                    crawlDelay: 60
                },
                {
                    userAgent: "OtherBot",
                    allow: ["/"],
                    crawlDelay: 60
                },
                {
                    userAgent: "*",
                    allow: "/",
                    crawlDelay: 60
                }
            ],
            sitemap: "http://localhost/sitemap.xml",
            host: "http://localhost"
        }),
    ]
};
