# Official base image
FROM node:14.11.0-alpine3.10

# Set working directory
WORKDIR /app
COPY package.json ./

# install app dependencies
RUN npm install --silent

# add app
COPY . .

EXPOSE 9000

# start app
CMD ["npm", "run", "start"]