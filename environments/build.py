"""Create environment variables.

What do you do need to build?
"""
import yaml
import sys

file = f'environments/{sys.argv[1]}/development/k8s/api-deployment.yaml'
image = f'gcr.io/{sys.argv[2]}/{sys.argv[3]}:{sys.argv[4]}'

with open(file) as file_name:
    data = yaml.safe_load(file_name)

# Set environment variables for deploy
(data['spec']['template']['spec']['containers'])[0]['image'] = image
((data['spec']['template']['spec']['containers'][0])['env'][0])['value'] = sys.argv[5]

dict_file = data

# Write file environment
with open(file, 'w') as file_name:
    data = yaml.dump(data, file_name, sort_keys=False)
