import React from "react";
import {HomeLayout} from "../components/layout/homeLayout";
import MapView from "../components/maps/mapView";
import map from "../assets/geojson/states.json"
import GetData from "../data/getData";
import CardSimple from "../components/cards/cardSimple";
import {connect} from "react-redux";
import PieChart from "../components/charts/pieChart";
import LinearChart from "../components/charts/linearChart";
import Title from "../components/messages/title";
import BarChart from "../components/charts/barChart";
import {Loader} from "../components/common/loader";

class Home extends React.Component {
    render() {
        var today = new Date();
        var year = today.getFullYear();
        var dashboard = this.props.data.data.length > 0 ?
            <div>
                <Title title={"Datos del: " + String(this.props.data.processed.date).substring(0, 10)}
                       subtitle="Los datos se actualizan cada dos días desde Serendipia"/>
                <div className="row my-auto mx-auto pb-3">
                    <div className="col-sm-2">
                        <CardSimple title="Total"
                                    content="Casos confirmados."
                                    value={this.props.data.processed.total}/>
                    </div>
                    <div className="col-sm-7">
                        <LinearChart title={'Casos confirmados mes a mes del ' + year}
                                     subtitle={'Confirmados en el tiempo (NOTA: El dataset COVIDMX modifica las fechas del dataset original)'}
                                     data={[
                                         ['Mes', 'Confirmados'],
                                         ["Enero", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][1].total : "Sin datos"],
                                         ["Febrero", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][2].total : "Sin datos"],
                                         ["Marzo", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][3].total : "Sin datos"],
                                         ["Abril", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][4].total : "Sin datos"],
                                         ["Mayo", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][5].total : "Sin datos"],
                                         ["Junio", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][6].total : "Sin datos"],
                                         ["Julio", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][7].total : "Sin datos"],
                                         ["Agosto", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][8].total : "Sin datos"],
                                         ["Septiembre", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][9].total : "Sin datos"],
                                         ["Octubre", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][10].total : "Sin datos"],
                                         ["Noviembre", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][11].total : "Sin datos"],
                                         ["Diciembre", this.props.data.processed[year] !== undefined ? this.props.data.processed[year]['month'][12].total : "Sin datos"],
                                     ]}/>
                    </div>
                    <div className="col-sm-3">
                        <PieChart data={
                            [
                                ['Sexo', 'Infectados'],
                                ['Mujeres', this.props.data.processed.f],
                                ['Hombres', this.props.data.processed.m],
                            ]
                        }/>
                    </div>
                </div>
                <div className="row my-auto mx-auto">
                    <div className="col-sm-6">
                        <BarChart data={
                            [
                                ['Rango de edad', 'Mujeres', 'Hombres'],
                                ['Niños 0-10', this.props.data.processed[year] !== undefined ? this.props.data.processed["0-10"].f : "Sin datos", this.props.data.processed["0-10"] !== undefined ? this.props.data.processed["0-10"].m : "Sin datos"],
                                ['Adolescentes 12-18', this.props.data.processed[year] !== undefined ? this.props.data.processed["12-18"].f : "Sin datos", this.props.data.processed["12-18"] !== undefined ? this.props.data.processed["12-18"].m : "Sin datos"],
                                ['Adultos jovenes 19-35', this.props.data.processed[year] !== undefined ? this.props.data.processed["19-35"].f : "Sin datos", this.props.data.processed["19-35"] !== undefined ? this.props.data.processed["19-35"].m : "Sin datos"],
                                ['Adultos 36-60', this.props.data.processed[year] !== undefined ? this.props.data.processed["36-60"].f : "Sin datos", this.props.data.processed["36-60"] !== undefined ? this.props.data.processed["36-60"].m : "Sin datos"],
                                ['Mayores de edad +60', this.props.data.processed[year] !== undefined ? this.props.data.processed["60-130"].f : "Sin datos", this.props.data.processed["60-130"] !== undefined ? this.props.data.processed["60-130"].m : "Sin datos"],
                            ]
                        }/>
                    </div>
                    <div className="col-sm-6">
                        <MapView
                            year={year}
                            map={map}
                            center={[25, -98]}
                            zoom={4}
                            url={'https://server.arcgisonline.com/ArcGIS/rest/services/Specialty/DeLorme_World_Base_Map/MapServer/tile/{z}/{y}/{x}'}/>
                    </div>
                </div>
            </div>
            :
            <Loader/>

        return (
            <HomeLayout
                props={this.props}
                title="Dashboard Covid"
            >
                <GetData map={map}/>
                {dashboard}
            </HomeLayout>
        )
    }
}

const mapStateToProps = (reducers) => {
    return reducers;
};

export default connect(mapStateToProps, null)(Home)