import {UPDATE_LABEL, GET_LABEL, UPDATE_OBJECT_LABEL} from '../types/index'

const initializeState = function () {
    return {
        label: "Selecciona un lugar en el mapa",
        object: undefined
    };
};

export const Label = (state = initializeState(), action) => {
    switch (action.type) {
        case UPDATE_LABEL: {
            return {
                ...state,
                label: action.payload.label,
            };
        }
        case UPDATE_OBJECT_LABEL: {
            return {
                ...state,
                object: action.payload.object,
            };
        }
        default:
            return state;
    }
};