import {combineReducers} from "redux";
import {Label} from './label';
import {Data} from "./data";

export default combineReducers({label: Label, data: Data});