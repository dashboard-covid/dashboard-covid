import {ADD_DATA, ADD_PROCESSED} from '../types/index'

const initializeState = function () {
    return {
        data: [],
        processed: []
    };
};

export const Data = (state = initializeState(), action) => {
    switch (action.type) {
        case ADD_DATA: {
            return {
                ...state,
                data: action.payload.covidData,
            };
        }
        case ADD_PROCESSED: {
            return {
                ...state,
                processed: action.payload.processed,
            };
        }
        default:
            return state;
    }
};