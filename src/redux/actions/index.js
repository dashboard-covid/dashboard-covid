import {UPDATE_LABEL, UPDATE_OBJECT_LABEL, ADD_DATA, ADD_PROCESSED} from '../types/index'

export const updateLabel = (data) => (dispatch) => {
    dispatch({
        type: UPDATE_LABEL,
        payload: data
    })
};

export const updateObjectLabel = (data) => (dispatch) => {
    dispatch({
        type: UPDATE_OBJECT_LABEL,
        payload: data
    })
};

export const addObjectData = (data) => (dispatch) => {
    dispatch({
        type: ADD_DATA,
        payload: data
    })
};

export const addObjectProcessed = (data) => (dispatch) => {
    dispatch({
        type: ADD_PROCESSED,
        payload: data
    })
};