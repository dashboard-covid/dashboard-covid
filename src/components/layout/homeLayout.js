import React, {Suspense} from "react";
import {Helmet} from "react-helmet";
import Header from "../common/header";
import Footer from "../common/footer";
import {Loader} from "../common/loader";

export const HomeLayout = ({
                               children,
                               title,
                           }) => {
    window.scrollTo(0, 0);
    return (
        <div>
            <Helmet>
                <title>{title}</title>
            </Helmet>
            <Header/>

            <div className="mt-5">
                <React.Suspense fallback={<Loader/>}>
                    {children}
                </React.Suspense>
            </div>
            <hr/>
            <Footer/>
        </div>
    );
};
