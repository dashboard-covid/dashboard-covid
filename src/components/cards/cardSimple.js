import React, {Component} from "react";

export default class CardSimple extends Component {
    render() {
        return (
            <div>
                <div className="container my-auto">
                    <section className="dark-grey-text text-center">
                        <h1>
                            {new Intl.NumberFormat("en-US").format(this.props.value)}
                        </h1>
                        <h3 className="font-weight-bold">
                            {this.props.title}
                        </h3>
                        <p className="text-muted">
                            {this.props.content}
                        </p>
                    </section>
                </div>
            </div>
        );
    }
}
