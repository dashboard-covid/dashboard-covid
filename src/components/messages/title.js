import React, {Component} from 'react';

class Title extends Component {

    render() {
        return (
            <>
                <div className="container pt-2">
                    <h2 className="text-center">{this.props.title}</h2>
                    <h5 className="text-center">{this.props.subtitle}</h5>
                </div>
            </>
        );
    }
}

export default Title;