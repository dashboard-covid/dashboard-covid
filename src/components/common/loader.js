import React from "react";
import {css} from "@emotion/core";
import {BounceLoader} from "react-spinners";
import Title from "../messages/title";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: 0 auto;
  border-color: #ffffff;
`;

export class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    render() {
        return (
            <div className="sweet-loading mt-5 pt-5 mb-5">
                <BounceLoader
                    css={override}
                    size={100}
                    color={"#0d796c"}
                    loading={this.state.loading}
                />
                <p className="text-center text-success">Cargando datos</p>
            </div>
        );
    }
}