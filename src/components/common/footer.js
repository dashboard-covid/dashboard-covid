import React, {Component} from "react";

export default class Footer extends Component {
    render() {
        return (
            <div className="text-center">
                <div className="container pb-3">
                    <div className="row">
                        <div className="col-12">
                            <span className="">@eocode {new Date().getFullYear()}</span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <p className="mx-auto">
                                Creando soluciones con{" "}
                                <span role="img" aria-labelledby="heart"> ❤</span>{" "}
                                desde México
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
