import React from "react";
import {Link} from "react-router-dom";
import {Navbar} from "react-bootstrap";

const Header = (props) => {
    return (
        <div className="container pb-3">
            <Navbar fixed="top" className="navbar bg-light text-dark">
                <Navbar.Brand className="mx-auto">
                    <Link to="/" className="text-black navbar-brand">
                        <span className="text-black">Dashboard de casos de COVID en México</span>
                    </Link>
                </Navbar.Brand>
            </Navbar>
        </div>
    );
};

export default Header