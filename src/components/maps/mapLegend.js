import {useLeaflet} from "react-leaflet";
import L from "leaflet";
import {useEffect} from "react";

const MapLegend = (props) => {
    const {map} = useLeaflet();

    useEffect(() => {
        // get color depending on population density value
        function getColor(d) {
            return d > props.processed['2020']['state']['max'] * .9 ? '#800026' :
                d > props.processed['2020']['state']['max'] * .8 ? '#BD0026' :
                    d > props.processed['2020']['state']['max'] * .6 ? '#E31A1C' :
                        d > props.processed['2020']['state']['max'] * .5 ? '#FC4E2A' :
                            d > props.processed['2020']['state']['max'] * .4 ? '#FD8D3C' :
                                d > props.processed['2020']['state']['max'] * .3 ? '#FEB24C' :
                                    d > props.processed['2020']['state']['max'] * .2 ? '#FED976' :
                                        '#FFEDA0';
        }

        const legend = L.control({position: "bottomleft"});

        legend.onAdd = () => {
            const div = L.DomUtil.create("div", "info legend");
            const grades = [0,
                parseInt(props.processed['2020']['state']['max'] * .2),
                parseInt(props.processed['2020']['state']['max'] * .3),
                parseInt(props.processed['2020']['state']['max'] * .4),
                parseInt(props.processed['2020']['state']['max'] * .5),
                parseInt(props.processed['2020']['state']['max'] * .6),
                parseInt(props.processed['2020']['state']['max'] * .8),
                parseInt(props.processed['2020']['state']['max'] * .9)];
            let labels = [];
            let from;
            let to;

            for (let i = 0; i < grades.length; i++) {
                from = grades[i];
                to = grades[i + 1];

                labels.push(
                    '<i style="background:' +
                    getColor(from + 1) +
                    '"></i> ' +
                    from +
                    (to ? "&ndash;" + to : "+")
                );
            }

            div.innerHTML = labels.join("<br>");
            return div;
        };

        legend.addTo(map);
    }, []);
    return null;
};

export default MapLegend;