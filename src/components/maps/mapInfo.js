import {useLeaflet} from "react-leaflet";
import L from "leaflet";
import React, {useEffect} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {updateObjectLabel} from "../../redux/actions";


const MapInfo = (props) => {
    const {map} = useLeaflet();

    function handleChangeLabel(object) {
        props.updateObjectLabel({object: object})
    }

    function addOrUpdate(info) {
        info.onAdd = () => {
            const div = L.DomUtil.create("div", "mapinfo");
            div.innerHTML = `<strong>Casos de COVID</strong> <br/>${props.label.label}`
            return div;
        };
        info.addTo(map);
    }

    useEffect(() => {
        if (props.label.object === undefined) {
            const info = L.control({position: "bottomright"});
            addOrUpdate(info)
            handleChangeLabel(info)
        } else {
            const info = props.label.object
            addOrUpdate(info)
        }
    }, [props]);
    return null;
};
const mapStateToProps = (reducers) => {
    return reducers;
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({updateObjectLabel}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MapInfo)