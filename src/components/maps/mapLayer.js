import React, {useRef} from 'react'
import {GeoJSON} from 'react-leaflet';
import L from 'leaflet';
import {bindActionCreators} from "redux";
import {updateLabel, updateObjectLabel} from "../../redux/actions";
import {connect} from "react-redux";
import {match_state} from "../../utils/utils";

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


const MapLayer = (props) => {

    const geo = useRef();

    function styleMap(feature) {
        return {
            fillColor: getColor(props.processed[props.year].state[match_state(feature.properties.state_name)].total),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        };
    }

    function getColor(d) {
        return d > props.processed['2020']['state']['max'] * .9 ? '#800026' :
            d > props.processed['2020']['state']['max'] * .8 ? '#BD0026' :
                d > props.processed['2020']['state']['max'] * .6 ? '#E31A1C' :
                    d > props.processed['2020']['state']['max'] * .5 ? '#FC4E2A' :
                        d > props.processed['2020']['state']['max'] * .4 ? '#FD8D3C' :
                            d > props.processed['2020']['state']['max'] * .3 ? '#FEB24C' :
                                d > props.processed['2020']['state']['max'] * .2 ? '#FED976' :
                                    '#FFEDA0';
    }

    function handleChangeLabel(label) {
        props.updateLabel({label: label})
    }

    function zoomToFeature(e) {
        props.geomap.current.leafletElement.fitBounds(e.target.getBounds());
    }

    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        handleChangeLabel(new Intl.NumberFormat("en-US").format(props.processed[props.year].state[match_state(layer.feature.properties.state_name)].total) + " casos confirmados en " + layer.feature.properties.state_name)
    }

    function resetHighlight(e) {
        geo.current.leafletElement.resetStyle(e.target);
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature,
        });
        // const popupContent = `<Popup>
        //                             <p>Centro: ${feature.properties.centro}</p>
        //                             <p>Tipo: ${feature.properties.tipocentro}</p>
        //                             <p>Titularidad: ${feature.properties.titularida}</p>
        //                         </Popup>`
        // layer.bindPopup(popupContent)
    }

    return <GeoJSON
        data={props.map}
        onEachFeature={onEachFeature}
        style={styleMap}
        ref={geo}
    />
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({updateLabel, updateObjectLabel}, dispatch)
}

export default connect(null, mapDispatchToProps)(MapLayer)