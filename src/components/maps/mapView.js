import React, {useRef} from "react";
import {Map as LeafMap, TileLayer, LayersControl} from "react-leaflet";
import MapLegend from "./mapLegend";
import MapInfo from "./mapInfo";
import MapLayer from "./mapLayer";
import {connect} from "react-redux";

const styles = {
    wrapper: {
        height: 320,
        width: '100%',
        margin: '0 auto',
        display: 'flex',
    },
};

const MapView = (props) => {
    const geomap = useRef();
    const {Overlay} = LayersControl;
    return (
        <LeafMap ref={geomap} style={styles.wrapper} center={props.center} zoom={props.zoom}>
            <LayersControl position="topright">
                <TileLayer url={props.url}/>
                <Overlay checked name="Casos de COVID">
                    <MapLayer processed={props.data.processed} year={props.year} map={props.map} geomap={geomap}/>
                    <MapLegend processed={props.data.processed}/>
                    <MapInfo/>
                </Overlay>
            </LayersControl>
        </LeafMap>
    );
}

const mapStateToProps = (reducers) => {
    return reducers;
};

export default connect(mapStateToProps, null)(MapView)