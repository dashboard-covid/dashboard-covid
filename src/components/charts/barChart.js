import React, {Component} from 'react'
import Chart from "react-google-charts";
import {Loader} from "../common/loader";

const BarChart = (props) => {
    return (
        <div>
            <Chart
                width={'100%'}
                height={'320px'}
                chartType="BarChart"
                loader={<Loader/>}
                data={props.data}
                options={{
                    title: 'Confirmados por edades',
                    chartArea: {width: '50%'},
                    hAxis: {
                        title: 'Confirmados',
                        minValue: 0,
                    },
                    vAxis: {
                        title: 'Edades',
                    },
                }}
                // For tests
                rootProps={{'data-testid': '1'}}
            />
        </div>
    );
}

export default BarChart;