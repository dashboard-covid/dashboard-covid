import React from 'react';
import Chart from "react-google-charts";
import {Loader} from "../common/loader";

const LinearChart = (props) => {
    return (
        <div>
            <Chart
                width={'100%'}
                height={'250px'}
                chartType="Line"
                loader={<Loader/>}
                data={props.data}
                options={{
                    chart: {
                        title: props.title,
                        subtitle: props.subtitle,
                    },
                }}
                rootProps={{'data-testid': '1'}}
            />
        </div>
    );
};

export default LinearChart;
