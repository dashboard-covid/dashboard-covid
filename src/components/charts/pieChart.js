import React from 'react';
import Chart from "react-google-charts";
import {Loader} from "../common/loader";

const PieChart = (props) => {
    return (
        <div className="text-center">
            <Chart
                width={'100%'}
                height={'200px'}
                chartType="PieChart"
                loader={<Loader/>}
                data={props.data}
                options={{}}
                rootProps={{'data-testid': '7'}}
            />
        </div>
    )
}

export default PieChart;