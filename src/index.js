import React from "react";
import ReactDOM from "react-dom";
import App from "./routes/app";
import {Provider} from 'react-redux'
import configureStore from './redux/store'
import "leaflet/dist/leaflet.css";
import "bootstrap/dist/css/bootstrap.css";
import "./assets/css/styles.css"


const store = configureStore()

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
)
;

if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
        navigator.serviceWorker
            .register("/sw.js")
            .then(registration => {
                console.log("SW registered: ", registration);
            })
            .catch(registrationError => {
                console.log("SW registration failed: ", registrationError);
            });
    });
}
