const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

export const match_state = (str) => {
    return removeAccents(str).replaceAll(" ", "_")
}