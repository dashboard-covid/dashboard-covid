import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addObjectData, addObjectProcessed} from "../redux/actions";
import {match_state} from "../utils/utils";

const GetData = (props) => {

    const getData = async () => {
        const response = await fetch(process.env.API)
        const covidData = await response.json()
        props.addObjectProcessed({processed: processData(covidData)})
        props.addObjectData({covidData})
    }

    useEffect(
        () => {
            getData()
        }, []
    )

    const processData = (covidData) => {
        let data = {}
        covidData.map((api, key) => {
            let type = api.type_info.split('_')
            // Add date extract
            if (data['date'] === undefined) {
                data['date'] = api.search_date
            }
            // Add global total
            if (data['total'] === undefined) {
                data['total'] = 0
            }
            // Add sex total
            if (data['f'] === undefined) {
                data['f'] = 0
            }
            if (data['m'] === undefined) {
                data['m'] = 0
            }
            // Add age total
            if (data[type[3]] === undefined) {
                data[type[3]] = {}
            }
            if (data[type[3]]['total'] === undefined) {
                data[type[3]]['total'] = 0
            }
            if (data[type[3]]['f'] === undefined) {
                data[type[3]]['f'] = 0
            }
            if (data[type[3]]['m'] === undefined) {
                data[type[3]]['m'] = 0
            }
            // Add year
            if (data[type[2]] === undefined)
                data[type[2]] = {}
            // Add Month
            if (data[type[2]]['month'] === undefined)
                data[type[2]]['month'] = {}
            if (data[type[2]]['month'][type[0]] === undefined)
                data[type[2]]['month'][type[0]] = {}
            // Add Month total
            if (data[type[2]]['month'][type[0]]['total'] === undefined)
                data[type[2]]['month'][type[0]]['total'] = 0
            // Add State
            if (data[type[2]]['state'] === undefined)
                data[type[2]]['state'] = {}
            // For each state
            props.map.features.map((state, key) => {
                const state_filter = match_state(state.properties.state_name)
                // Add states
                if (data[type[2]]['state'][state_filter] === undefined)
                    data[type[2]]['state'][state_filter] = {}
                // Sum state max
                if (data[type[2]]['state']['max'] === undefined)
                    data[type[2]]['state']['max'] = 0
                // Sum state total
                if (data[type[2]]['state'][state_filter]['total'] === undefined)
                    data[type[2]]['state'][state_filter]['total'] = 0
                if (api[state_filter] !== undefined)
                    data[type[2]]['state'][state_filter]['total'] = data[type[2]]['state'][state_filter]['total'] + api[state_filter]
                if (api[state_filter] !== undefined && data[type[2]]['state'][state_filter]['total'] > data[type[2]]['state']['max'])
                    data[type[2]]['state']['max'] = data[type[2]]['state'][state_filter]['total']
                // Add Month States
                if (data[type[2]]['month'][type[0]][state_filter] === undefined)
                    data[type[2]]['month'][type[0]][state_filter] = {}
                // Add State total
                if (data[type[2]]['month'][type[0]][state_filter]['total'] === undefined)
                    data[type[2]]['month'][type[0]][state_filter]['total'] = 0
                // Add Age
                if (data[type[2]]['month'][type[0]][state_filter]['age'] === undefined)
                    data[type[2]]['month'][type[0]][state_filter]['age'] = {}
                // Add Age clasification
                if (data[type[2]]['month'][type[0]][state_filter]['age'][type[3]] === undefined)
                    data[type[2]]['month'][type[0]][state_filter]['age'][type[3]] = 0
                // Add Sex
                if (data[type[2]]['month'][type[0]][state_filter]['sex'] === undefined)
                    data[type[2]]['month'][type[0]][state_filter]['sex'] = {}
                // Add Sex Genre
                if (data[type[2]]['month'][type[0]][state_filter]['sex'][type[4]] === undefined)
                    data[type[2]]['month'][type[0]][state_filter]['sex'][type[4]] = 0
                // Sum global total
                if (api[state_filter] !== undefined)
                    data['total'] = data['total'] + api[state_filter]
                // Sum month total
                if (api[state_filter] !== undefined)
                    data[type[2]]['month'][type[0]]['total'] = data[type[2]]['month'][type[0]]['total'] + api[state_filter]
                // Sum genre total
                if (api[state_filter] !== undefined) {
                    data[type[3]]['total'] = data[type[3]]['total'] + api[state_filter]
                }
                if (api[state_filter] !== undefined && type[4] === 'F') {
                    data[type[3]]['f'] = data[type[3]]['f'] + api[state_filter]
                }
                if (api[state_filter] !== undefined && type[4] === 'M') {
                    data[type[3]]['m'] = data[type[3]]['m'] + api[state_filter]
                }
                // Sum global sex
                if (api[state_filter] !== undefined && type[4] === 'F')
                    data['f'] = data['f'] + api[state_filter]
                if (api[state_filter] !== undefined && type[4] === 'M')
                    data['m'] = data['m'] + api[state_filter]
                // Sum state total
                if (api[state_filter] !== undefined && data[type[2]]['month'][type[0]][state_filter]['total'] !== undefined)
                    data[type[2]]['month'][type[0]][state_filter]['total'] = data[type[2]]['month'][type[0]][state_filter]['total'] + api[state_filter]
                // Sum state age total
                if (api[state_filter] !== undefined && data[type[2]]['month'][type[0]][state_filter]['age'][type[3]] !== undefined)
                    data[type[2]]['month'][type[0]][state_filter]['age'][type[3]] = data[type[2]]['month'][type[0]][state_filter]['age'][type[3]] + api[state_filter]
                // Sum state sex total
                if (api[state_filter] !== undefined && data[type[2]]['month'][type[0]][state_filter]['sex'][type[4]] !== undefined)
                    data[type[2]]['month'][type[0]][state_filter]['sex'][type[4]] = data[type[2]]['month'][type[0]][state_filter]['sex'][type[4]] + api[state_filter]
            })
        })
        return data
    }

    return null;
};

const mapStateToProps = (reducers) => {
    return reducers;
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({addObjectData, addObjectProcessed}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(GetData)
